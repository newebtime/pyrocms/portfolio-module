<?php

namespace Newebtime\PortfolioModule\Http\Controller\Admin;

use Illuminate\Routing\Redirector;
use Newebtime\PortfolioModule\Project\Contract\ProjectRepositoryInterface;
use Newebtime\PortfolioModule\Project\Form\ProjectFormBuilder;
use Newebtime\PortfolioModule\Project\Table\ProjectTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

/**
 * Class ProjectsController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectsController extends AdminController
{
    /**
     * Display an index of existing entries.
     *
     * @param ProjectTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ProjectTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ProjectFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ProjectFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ProjectFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ProjectFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    /**
     * @param ProjectRepositoryInterface $projects
     * @param Redirector $redirect
     * @param $id
     * @return mixed
     */
    public function view(ProjectRepositoryInterface $projects, Redirector $redirect, $id)
    {
        $project = $projects->find($id);

        if ($project->services->count() != 0) {
            return $redirect->to(route('newebtime.module.portfolio::projects.show', ['projectSlug' => $project->slug]));
        } elseif ($project->client) {
            return $redirect->to(route('newebtime.module.portfolio::customers.project', ['clientSlug' => $project->client->slug, 'projectSlug' => $project->slug]));
        }

        return abort(404);
    }
}
